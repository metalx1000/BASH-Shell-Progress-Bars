#!/bin/bash
######################################################################
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

#starting count
count=0

#get a count of all files
total=$(find 2>/dev/null |wc -l)
echo "Total Files $total"

#loop through each file
find 2>/dev/null |while read f;
do
  #add one to the count for each file we find
  count=$((count+1))

  #add the file name to a log file
  echo "$f" >> /tmp/file.log
  #echo the current percent of the total number of files
  echo $(( 100*$count/$total ))

  #pipe it into 'dialog' to update progress bar
done|dialog --title "Logging All Files" --gauge "Please wait ...." 10 60 0


